import { Directive,ElementRef , HostListener,Input} from '@angular/core';

@Directive({
  selector: '[pkmnBorderCard]'
})
export class BorderCardDirective {

  @Input('pkmnBorderCard') borderColor :string;
  private initialHeight = 180;
  private initialBorder = "#f5f5f5";
  private defaultBorder = "#009688";

  constructor(private el : ElementRef) { 

    this.setBorder(this.initialBorder);
    this.setHeight(this.initialHeight);
  }


  @HostListener('mouseenter') onMouseEnter() {
    this.setBorder(this.borderColor || this.defaultBorder);
  }
  
  @HostListener('mouseleave') onMouseLeave() {
    this.setBorder('#f5f5f5');
  }



  private setBorder(color : string){
    this.el.nativeElement.style.border = '4px solid ' + color;
  }

  private setHeight(height : number){
    this.el.nativeElement.style.height = height +'px';
  }

}
