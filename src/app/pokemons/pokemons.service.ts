import { Injectable } from '@angular/core';
import { Pokemon } from './pokemon';
import { POKEMONS } from './mock-pokemons';
@Injectable()

export class PokemonsService {

    getPokemons() : Pokemon[] {
        return  POKEMONS;
    }

    getPokemon (id: number) : Pokemon {
        const pokemon = POKEMONS.filter(pok => pok.id = id );
        return pokemon[0];
    } 


    getPokemonTypes () : string[]{
        return ['feu', 'Electrik', 'Fée', 'Feu', 'Poison', 'Plante','Insecte', 'Normal', 'Vol'];
    }
}